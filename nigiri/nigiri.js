/*global module, require */

const path = require("path");
const os = require("os");
const fs = require("fs");

const randomstring = require("randomstring");

// Puppeteer works in alpine, playwright does not
// https://github.com/microsoft/playwright/issues/1986
const puppeteer = require("puppeteer");

const DEFAULT_PAGE_NAVIGATION_TIMEOUT_MS = 60000; // 60 seconds
const DEFAULT_VISIBILITY_TIMEOUT_MS = 5000; // 5 seconds
const DEFAULT_PAGE_WAIT_TIMEOUT_MS = 3000; // 3 seconds

const DEFAULT_NIGIRI_BASE_URL = "https://www.ginsara.jp";

const GNS_PPTR_POSTAL_LINK_SELECTOR = "a#postal[data-ratid=top_Postcode]";
const GNS_PPTR_POSTAL_INPUT_SELECTOR = "input.c-search-input.js-address-postcode";
const GNS_PPTR_FIRST_ZIPCODE_SEARCH_RESULT_BUTTON_SELECTOR = "ul.c-search-suggest > li > button:nth-child(1)";
const GNS_PPTR_ZIPCODE_MODAL_IMMEDIATE_DELIVERY_BUTTON_SELECTOR = "a#deliveryAddressModal-postCode-immediateButton";
const GNS_PPTR_STORE_SEARCH_PAGE_SPAN_SELECTOR = "span#storeSearchPage-deliveryDateTimeBar";

const GNS_PPTR_LOGIN_EMAIL_INPUT_SELECTOR = "input#email";
const GNS_PPTR_LOGIN_PASSWORD_INPUT_SELECTOR = "input#password";
const GNS_PPTR_LOGIN_BUTTON_SELECTOR = "a#js-login-submit";

const GNS_PPTR_MY_PAGE_BUTTON_XPATH_SELECTOR = "//a[contains(., 'マイページ')]";
const GNS_PPTR_LOG_IN_BUTTON_XPATH_SELECTOR = "//a[contains(., 'ログインする')]";
const GNS_PPTR_CONFIRM_DELIVERY_ADDRESS_XPATH_SELECTOR = "//a[contains(., 'お届け先を確定して次へ')]";

const GNS_PPTR_DELIVERY_TIME_HEADER_XPATH_SELECTOR = "//h1[contains(., 'お届け日時を指定する')]";
const GNS_PPTR_OUTSIDE_SERVICE_TIME_WARNING = "//p[contains(text(), 'ただいまの時間は営業時間外のため、予約注文のみ受け付けております')]";

const GNS_PPTR_IMMEDIATE_DELIVERY_BUTTON_XPATH_SELECTOR = "//a[contains(., '今すぐお届け ')]";
const GNS_PPTR_ITEM_SELECTION_HEADER_XPATH_SELECTOR = "//h1[contains(., '商品を選ぶ')]";
const GNS_PPTR_CONFIRM_PAYMENT_DETAILS_HEADER_XPATH_SELECTOR = "//h1[contains(., '注文情報を入力する')]";
const GNS_PPTR_PLACE_ORDER_HEADER_XPATH_SELECTOR = "//h1[contains(., '注文情報を確認する')]";

const GNS_PPTR_ADD_TO_CART_BUTTON_XPATH_SELECTOR = "//a[contains(., 'カートに入れる')]";
const GNS_PPTR_CONFIRM_CART_BUTTON_XPATH_SELECTOR = "//span[contains(.,'商品の内容を確認して次へ')]";
const GNS_PPTR_CONFIRM_PAYMENT_DETAILS_BUTTON_XPATH_SELECTOR = "//a[contains(., '支払い方法を確認して次へ')]";
const GNS_PPTR_PLACE_ORDER_BUTTON_XPATH_SELECTOR = "//a[contains(., '注文を確定する')]";

const GNS_PPTR_CART_ITEM_COUNT_BADGE_CSS_SELECTOR = "span.p-floatMenuNavigation__buttonCount.js-quantity-area";

const GNS_PPTR_REUSABLE_BOX_DELIVERY_XPATH_SELECTOR = "//span[contains(., '回収容器')]";

/**
 * Check whether an XPath selector is visible
 *
 * @param {Page} page - Puppeteer page
 * @param {Page} [xpath] - XPath selector
 * @param {Page} [css] - CSS selector
 * @param {Page} [timeoutMs] - selector
 * @returns {boolean}
 */
async function isVisible({ page, xpath, css, timeoutMs }) {
  try {
    if (xpath) { await page.waitForXPath(xpath, { visible: true, timeout: timeoutMs || DEFAULT_VISIBILITY_TIMEOUT_MS }); }
    if (css) { await page.waitForSelector(css, { visible: true, timeout: timeoutMs || DEFAULT_VISIBILITY_TIMEOUT_MS }); }
    return true;
  } catch {
    return null;
  }
}

/**
 * Take a screenshot of the page
 *
 * @param {string} tmpDir - path to the temporary directory in which to store the screenshot
 * @param {Page} page
 * @param {Node} node
 * @param {string} runId
 * @param {string} shotName - name of the screen shot, without extension. ("<name>.screenshot.png" will be the eventual file output name)
 * @param {'image' | 'pdf'} format
 * @returns {Promise<void>} A promise that returns when the screenshot has been taken
 */
async function saveScreenshot({ tmpDir, page, node, runId, shotName, format }) {
  format = format || "pdf";

  let outputPath = path.join(tmpDir, `${shotName}.screenshot`);

  switch (format) {
  case "pdf":
    outputPath += ".pdf";
    await page.pdf({
      path: outputPath,
      displayHeaderFooter: true,
      headerTemplate: '',
      footerTemplate: '',
      printBackground: true,
      format: 'A4',
    });
    break;

  case "image":
    outputPath += ".png";
    await page.screenshot({ path: outputPath });
    break;

  default:
    throw new Error(`unrecognized format [${format}]`);
  }

  if (node && node.log) {
    node.log(`[run ${runId || '?'}] successfully recorded current page @ [${outputPath}]`);
  }
}

/**
 * Retrieve the numeric value of the text in a selector
 *
 * @param {page} page
 * @param {string} [css] - CSS selector
 * @param {string} [xpath] - XPATH selector
 * @param {number} [timeoutMs] - Timeout to use if sepcified
 * @returns {Promise<number>} A promise that resolves to the parse dnumber
 */
async function getNumericValue({ page, css, xpath, timeoutMs }) {
  if (!xpath && !css) { throw new Error("Selector not provided, must specify either css or XPath"); }

  let elem;
  let timeout = timeoutMs || DEFAULT_VISIBILITY_TIMEOUT_MS;
  if (xpath) { elem = await page.waitForXPath(xpath, { visible: true, timeout }); }
  if (css) { elem = await page.waitForSelector(css, { visible: true, timeout }); }

  const jsHandle = await elem.getProperty('textContent');
  const value = await jsHandle.jsonValue();
  const parsed = parseInt(value, 10);

  if (isNaN(parsed)) { throw Error(`Failed to parse numeric value from DOM [${value}]`); }

  return parsed;
}

/**
 * Check whether the current page is a 404 page
 *
 * @param {object} args
 * @param {Page} args.page - Puppeteer page
 * @returns {boolean}
 */
async function is404Page({ page }) {
  const content = await page.content();
  return content.match("お探しのページが見つかりません");
}

/**
 * Scroll to the bottom of the given page
 *
 * @param {object} args
 * @param {Page} args.page - Puppeteer page
 * @returns {boolean}
 */
async function scrollToBottom({ page }) {
  const distance = 100; // should be less than or equal to window.innerHeight
  const delay = 250;
  while (await page.evaluate(() => document.scrollingElement.scrollTop + window.innerHeight < document.scrollingElement.scrollHeight)) {
    await page.evaluate((y) => { document.scrollingElement.scrollBy(0, y); }, distance);
    await page.waitForTimeout(delay);
  }
}

/**
 * Input handler for the Nigiri Node
 *
 * @param {any} args
 * @param {any} args.node - NodeRED Node object
 * @param {any} args.msg - Input for node
 * @returns {(msg) => Promise<void | Error>} A Promise that resolves when the message has been processed
 */
async function nodeOnInput(args) {
  const { node, msg, credentials } = args;
  let err;

  // Generate an ID for this particular run/attempt
  const runId = randomstring.generate(5);

  // Ensure Node is present
  if (!node) {
    err = new Error("Node object not provided");
    return err;
  }

  // Ensure puppeteer browser has already been instantiated
  if (!node.puppeteer || !node.puppeteer.browser) {
    err = new Error("Missing/Invalid puppeteer and/or browser instance");
    node.error(err, msg);
    return err;
  }
  node.log(`[run ${runId}] waiting for puppeteer browser install...`);
  const browser = await node.puppeteer.browser;

  // Ensure storeID has been set
  if (!node.storeID) {
    err = new Error(`Missing/Invalid Nigiri storeID [${node.storeID}]`);
    node.error(err, msg);
    return err;
  }
  const storeID = node.storeID;

  // Ensure email has been set
  if (!credentials.gnsEmail) {
    err = new Error(`Missing/Invalid credential gnsEmail [${credentials.gnsEmail}]`);
    node.error(err, msg);
    return err;
  }

  // Ensure password has been set
  if (!credentials.gnsPassword) {
    err = new Error(`Missing/Invalid credential gnsPassword [${credentials.gnsPassword}]`);
    node.error(err, msg);
    return err;
  }

  // Ensure productID has been set
  if (!node.productID) {
    err = new Error(`Missing/Invalid Nigiri productID [${node.productID}]`);
    node.error(err, msg);
    return err;
  }
  const productID = node.productID;

  // If the order is coming from the web (not a button press), we need to do some validation
  if (msg && msg.req) {
    const headerValue = msg.req.headers["authorization"];
    const expected = `bearer ${credentials.bearerToken}`;
    if (headerValue !== expected) {
      err = new Error(`Missing/Invalid shared secret, does not match configured value`);
      node.error(err, msg);
      return err;
    }
  }
  // Create the temporary directory for storing run progress
  let tmpDir;
  try {
    const prefix = path.join(os.tmpdir(), `nodered-nigiri-run-${runId}`);
    // tmpDir = await fs.promises.mkdtemp(prefix); // TODO: REMOVE
    tmpDir = prefix;
    if (!fs.existsSync(tmpDir)) {
      await fs.promises.mkdir(tmpDir);
    }
    if (!tmpDir) { throw new Error(`failed to make temporary directory @ prefix [${prefix}]`); }
  } catch (err) {
    node.error(err);
    return err;
  }

  let screenshotPaths = [];

  try {
    // Create new browser page
    const page = await browser.newPage();
    node.log(`[run ${runId}] created new browser page [${node.nigiriBaseURL}]`);

    // Navigate to Gin no Sara main page
    await page.goto(node.nigiriBaseURL, { timeout: DEFAULT_PAGE_NAVIGATION_TIMEOUT_MS });
    node.log(`[run ${runId}] successfully to nigiri baseURL [${node.nigiriBaseURL}]`);

    // Screenshot: landing page
    screenshotPaths.push(
      await saveScreenshot({ tmpDir, page, node, runId, shotName: "landing-page" }),
    );

    // Attempt to find the visible "my page" button (only shows if logged in)
    let loggedIn = await isVisible({ page, xpath: GNS_PPTR_MY_PAGE_BUTTON_XPATH_SELECTOR });
    if (!loggedIn) {
      node.log(`[run ${runId}] not logged in, logging in user....`);

      // Get the login button
      const [ loginButton ] = await page.$x(GNS_PPTR_LOG_IN_BUTTON_XPATH_SELECTOR);

      // Navigate to login page
      const loginURL = `${node.nigiriBaseURL}/session/create`;
      await page.goto(loginURL, { timeout: DEFAULT_PAGE_NAVIGATION_TIMEOUT_MS });
      node.log(`[run ${runId}] successfully navigated to login URL [${loginURL}]`);

      // Enter username and password
      await page.waitForSelector(GNS_PPTR_LOGIN_EMAIL_INPUT_SELECTOR);
      await page.type(GNS_PPTR_LOGIN_EMAIL_INPUT_SELECTOR, credentials.gnsEmail);
      await page.waitForSelector(GNS_PPTR_LOGIN_PASSWORD_INPUT_SELECTOR);
      await page.type(GNS_PPTR_LOGIN_PASSWORD_INPUT_SELECTOR, credentials.gnsPassword);

      // Screenshot: login credentials entry
      screenshotPaths.push(
        await saveScreenshot({ tmpDir, page, node, runId, shotName: "login-creds-entry" }),
      );

      // Click Login
      await page.click(GNS_PPTR_LOGIN_BUTTON_SELECTOR);
      loggedIn = await isVisible({ page, xpath: GNS_PPTR_MY_PAGE_BUTTON_XPATH_SELECTOR });
      if (!loggedIn) {
        err = new Error(`Failed to login to Gin no Sara`);
        node.error(err, msg);
        return err;
      }

      // Screenshot: post-login
      screenshotPaths.push(
        await saveScreenshot({ tmpDir, page, node, runId, shotName: "post-login" }),
      );
    }

    // Go to the orders page
    const orderStartURL = `${node.nigiriBaseURL}/orders/customer_types`;
    node.log(`[run ${runId}] navigating to order start URL [${orderStartURL}]`);
    await page.goto(orderStartURL, { timeout: DEFAULT_PAGE_NAVIGATION_TIMEOUT_MS });

    // Look for buttons that indicate that an address is already chosen
    const preRegisteredAddressSignals = await Promise.all([
      isVisible({ xpath: `//h2[contains(., 'アドレス帳から選ぶ')]` }),
      isVisible({ xpath: `//p[contains(., '選択中')]` }),
      isVisible({ css: `a.js-shop-search-trigger[value=1][data-address-shop-cd=${storeID}]` }),
    ]);
    if (!preRegisteredAddressSignals.some(v => !v)) {
      node.log(`[run ${runId}] statuses for pre-registered address checks [${preRegisteredAddressSignals}]`);
      err = new Error(`Failed to find pre-registered address on delivery address (one or more checks failed) selection screen!`);
      node.error(err, msg);
      return err;
    }

    // Screenshot: address choice
    screenshotPaths.push(
      await saveScreenshot({ tmpDir, page, node, runId, shotName: "address-choice" }),
    );

    // Choose pre-existing address selection
    const [ confirmAddressButton ] = await page.$x(GNS_PPTR_CONFIRM_DELIVERY_ADDRESS_XPATH_SELECTOR);
    await confirmAddressButton.click();

    // Wait for header for delivery time header to show up
    await page.waitForXPath(GNS_PPTR_DELIVERY_TIME_HEADER_XPATH_SELECTOR);

    // Screenshot: delivery time choice page
    screenshotPaths.push(
      await saveScreenshot({ tmpDir, page, node, runId, shotName: "delivery-time-choice" }),
    );

    // Detect if we can't place our order immediately
    let outOfServiceTime = await isVisible({ page, xpath: GNS_PPTR_OUTSIDE_SERVICE_TIME_WARNING });
    if (outOfServiceTime) {
      node.log(`[run ${runId}] detected out of service time warning`);
      err = new Error(`The selected restaurant cannot deliver immediately (@ ${new Date().toLocaleString()})`);
      node.error(err, msg);
      return err;
    }

    // Find & click the immediate delivery button
    const [ immediateDeliveryButton ] = await page.$x(GNS_PPTR_IMMEDIATE_DELIVERY_BUTTON_XPATH_SELECTOR);
    await immediateDeliveryButton.click();

    // Wait for item selection header
    await page.waitForXPath(GNS_PPTR_ITEM_SELECTION_HEADER_XPATH_SELECTOR);
    screenshotPaths.push(
      await saveScreenshot({ tmpDir, page, node, runId, shotName: "item-selection-screen" }),
    );

    // Visit the store page (not strictly necessary)
    const storeURL = `${node.nigiriBaseURL}/shops/${storeID}`;
    node.log(`[run ${runId}] navigating to URL for store [${storeID}]: [${storeURL}]`);
    await page.goto(storeURL, { timeout: DEFAULT_PAGE_NAVIGATION_TIMEOUT_MS });

    screenshotPaths.push(
      await saveScreenshot({ tmpDir, page, node, runId, shotName: "shop-page" }),
    );

    // Visit the product description page
    const productURL = `${node.nigiriBaseURL}/orders/products/${productID}`;
    node.log(`[run ${runId}] navigating to listing page for product [${productID}]: [${productURL}]`);
    await page.goto(productURL, { timeout: DEFAULT_PAGE_NAVIGATION_TIMEOUT_MS });

    screenshotPaths.push(
      await saveScreenshot({ tmpDir, page, node, runId, shotName: "product-page" }),
    );

    // Check to ensure the product is present and not a 404
    const productPageMissing = await is404Page({ page });
    if (productPageMissing) {
      node.log(`[run ${runId}] detected missing product page for product ID [${productID}]`);
      err = new Error(`The product page for productID [${productID}] is missing! Please configure another product ID`);
      node.error(err, msg);
      return err;
    }

    // Get the current count of items
    const beforeItemCount = await getNumericValue({ page, css: GNS_PPTR_CART_ITEM_COUNT_BADGE_CSS_SELECTOR });

    // Click the add to cart button
    const [ addToCartButton ] = await page.$x(GNS_PPTR_ADD_TO_CART_BUTTON_XPATH_SELECTOR);
    await addToCartButton.click();

    // Wait for the page to settle and JS to execute, network requests to update the count
    await page.waitForTimeout(DEFAULT_PAGE_WAIT_TIMEOUT_MS);

    // Re-retrieve the count afterwards
    const afterItemCount = await getNumericValue({ page, css: GNS_PPTR_CART_ITEM_COUNT_BADGE_CSS_SELECTOR });
    if (afterItemCount !== beforeItemCount + 1) {
      node.log(`[run ${runId}] failed to detect successful addition to cart`);
      err = new Error(`The number of items in the cart wasn't updated! It went from [${beforeItemCount}] to [${afterItemCount}]`);
      node.error(err, msg);
      return err;
    }

    screenshotPaths.push(
      await saveScreenshot({ tmpDir, page, node, runId, shotName: "product-page-after-add-to-cart" }),
    );

    // Navigate to cart page
    const cartURL = `${node.nigiriBaseURL}/orders/cart`;
    node.log(`[run ${runId}] navigating to listing cart URL: [${cartURL}]`);
    await page.goto(cartURL, { timeout: DEFAULT_PAGE_NAVIGATION_TIMEOUT_MS });

    screenshotPaths.push(
      await saveScreenshot({ tmpDir, page, node, runId, shotName: "cart-review" }),
    );

    // Select the option for a non-throwaway box ♻
    node.log(`[run ${runId}] selecting multi-use delivery box...`);
    const [ selectReusableBoxButton ] = await page.$x(GNS_PPTR_REUSABLE_BOX_DELIVERY_XPATH_SELECTOR);
    await selectReusableBoxButton.click();

    // Click confirm cart contents button
    node.log(`[run ${runId}] confirming cart...`);
    const [ confirmCartButton ] = await page.$x(GNS_PPTR_CONFIRM_CART_BUTTON_XPATH_SELECTOR);
    await confirmCartButton.click();

    // Wait for order place order header
    await page.waitForXPath(GNS_PPTR_CONFIRM_PAYMENT_DETAILS_HEADER_XPATH_SELECTOR);

    screenshotPaths.push(
      await saveScreenshot({ tmpDir, page, node, runId, shotName: "payment-details-confirmation", format: "image" }),
    );

    // As it can take a while for this button to show up (JS setup?)
    // let's wait until it's visible
    const confirmPaymentDetailsButtonVisible = await isVisible({ page, xpath: GNS_PPTR_CONFIRM_PAYMENT_DETAILS_BUTTON_XPATH_SELECTOR });
    if (!confirmPaymentDetailsButtonVisible) {
      node.log(`[run ${runId}] confirm payments button never became visible`);
      err = new Error(`The confirm payments button never became visible! Please submit an issue/check related RPA code`);
      node.error(err, msg);
      return err;
    }

    // Scroll to the bottom of the page
    await scrollToBottom({ page });

    // Click confirm payment details button
    node.log(`[run ${runId}] confirming payment details...`);
    const [ confirmPaymentDetailsButton ] = await page.$x(GNS_PPTR_CONFIRM_PAYMENT_DETAILS_BUTTON_XPATH_SELECTOR);
    await confirmPaymentDetailsButton.click();

    // Wait for the page to settle and JS to execute, network requests to update the count
    await page.waitForTimeout(DEFAULT_PAGE_WAIT_TIMEOUT_MS);

    screenshotPaths.push(
      await saveScreenshot({ tmpDir, page, node, runId, shotName: "after-confirm-payment-details", format: "image" }),
    );

    // Wait for order place order header
    await page.waitForXPath(GNS_PPTR_PLACE_ORDER_HEADER_XPATH_SELECTOR);

    screenshotPaths.push(
      await saveScreenshot({ tmpDir, page, node, runId, shotName: "order-confirmation" }),
    );

    if (node.dryRun) {
      node.log(`[run ${runId}] avoiding placing order (dry run)`);
      return node.send({
        payload: {
          meta: {
            runId,
            screenshotPaths,
            isDryDrun: true,
          },
        },
      });
    }

    // As it can take a while for this button to show up (JS setup?)
    // let's wait until it's visible
    const placeOrderButtonVisible = await isVisible({ page, xpath: GNS_PPTR_PLACE_ORDER_BUTTON_XPATH_SELECTOR });
    if (!placeOrderButtonVisible) {
      node.log(`[run ${runId}] place order button never became visible`);
      err = new Error(`The place order button never became visible! Please submit an issue/check related RPA code`);
      node.error(err, msg);
      return err;
    }

    // Scroll to the bottom of the page
    await scrollToBottom({ page });

    // Click place order button
    node.log(`[run ${runId}] placing order...`);
    const [ placeOrderButton ] = await page.$x(GNS_PPTR_PLACE_ORDER_BUTTON_XPATH_SELECTOR);
    await placeOrderButton.click();

    screenshotPaths.push(
      await saveScreenshot({ tmpDir, page, node, runId, shotName: "order-placed" }),
    );
    node.log(`[run ${runId}] successfully placed order`);

    // Return lots of information about the action
    return node.send({
      payload: {
        meta: {
          runId,
          screenshotPaths,
        },
      },
    });

  } catch (err) {
    node.error(err, msg);
    return err;
  }
}

/**
 * NodeRED module wrapper
 *
 * @param {any} RED - nodered context
 */
function nr(RED) {
  // Dependencies

  /**
   * Nigiri: Sushi web ordering automation NodeRED custom node
   *
   * @class NigiriNode
   */
  function NigiriNode(config) {
    // NodeRED init
    RED.nodes.createNode(this, config);
    const node = this;

    node.name = config.name;
    node.nigiriBaseURL = config.nigiriBaseURL || DEFAULT_NIGIRI_BASE_URL;
    node.storeID = config.storeID;
    node.productID = config.productID;
    node.dryRun = config.dryRun;

    // Puppeteer setup & config
    node.puppeteer = {};

    node.puppeteer.config = {
      windowSize: {
        widthPx: 1920,
        heightPx: 1080,
      }
    };

    node.puppeteer.browser = new Promise((resolve, reject) => {
      puppeteer
        .launch({
          // safe sandboxed chromium not supported yet
          args: [
            '--no-sandbox',
            `--window-size=${node.puppeteer.config.windowSize.widthPx},${node.puppeteer.config.windowSize.heightPx}`,
          ],
        })
        .then(browser => {
          node.puppeteer.browser = browser;

          // Setup node input handler
          node.on('input', function(msg) {
            nodeOnInput({ node, msg, credentials: node.credentials });
          });
        })
        .catch(reject);
    });

  }

  // Register the node
  RED.nodes.registerType(
    "nigiri",
    NigiriNode,
    {
      credentials: {
        gnsEmail: { type: "text" },
        gnsPassword: { type: "password" },
        bearerToken: { type: "password" },
      },
    }
  );
}

module.exports = nr;

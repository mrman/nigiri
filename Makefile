.PHONY: all \
				check-tool-docker check-tool-entr \
# Images
				nr-builder-image \
				nr-image nr-image-publish \
# NR local
				nr-local-data-dir nr-local-stop \
				nr-local-remove nr-local nr-local-shell \
# FlowForge
				ff-nr-image ff-nr-local-data-dir \
				ff-nr-local-stop ff-nr-local-remove \
				ff-nr-local ff-nr-local-shell

DOCKER ?= docker
ENTR ?= entr

all:
	$(error "NOT IMPLEMENTED")

###########
# Tooling #
###########

## Ensure docker is installed
ensure-tool-docker:
ifeq (,$(shell which $(DOCKER)))
	$(error "Docker does not seem to be installed (see: https://docs.docker.io)")
endif

## Ensure entr is installed
ensure-tool-entr:
ifeq (,$(shell which $(ENTR)))
	$(error "entr does not seem to be installed (see: http://eradman.com/entrproject)")
endif

#################
# NodeRED Local #
#################
# Run a basic "stock" NodeRED installation locally
#

NR_BUILDER_DOCKERFILE_PATH ?= $(realpath infra/docker/nr.builder.Dockerfile)
NR_BUILDER_IMAGE ?= nr-builder
NR_BUILDER_IMAGE_VERSION ?= 2.2.2-12
NR_BUILDER_IMAGE_FULL ?= $(NR_BUILDER_IMAGE):$(NR_BUILDER_IMAGE_VERSION)

NR_DOCKERFILE_PATH ?= $(realpath infra/docker/nr.Dockerfile)
NR_IMAGE ?= nr
NR_IMAGE_VERSION ?= 2.2.2-12
NR_IMAGE_FULL ?= $(NR_IMAGE):$(NR_IMAGE_VERSION)

NR_LOCAL_PORT ?= 1880
NR_LOCAL_CONTAINER_NAME ?= nr-local

NR_LOCAL_DATA_DIR_PATH ?= ./local-data/nr-local
NR_LOCAL_DATA_DIR ?= $(realpath .)/$(NR_LOCAL_DATA_DIR_PATH)

nr-builder-image:
	$(DOCKER) build -f $(NR_BUILDER_DOCKERFILE_PATH) -t $(NR_BUILDER_IMAGE_FULL) .

nr-image:
	$(DOCKER) build -f $(NR_DOCKERFILE_PATH) -t $(NR_IMAGE_FULL) .

nr-image-publish:
	$(DOCKER) push $(NR_IMAGE_FULL)

## Ensure local data directory for node red exists
nr-local-data-dir:
	@echo "=> Ensuring local data dir for nodered @ [$(NR_LOCAL_DATA_DIR)]..."
	@mkdir -p $(NR_LOCAL_DATA_DIR_PATH)

## Stop local node red
nr-local-stop:
	@echo "=> Stopping docker container [$(NR_LOCAL_CONTAINER_NAME)]..."
	@$(DOCKER) stop $(NR_LOCAL_CONTAINER_NAME) || true

## Remove local node-red container
nr-local-remove:
	@echo "=> Removing docker container [$(NR_LOCAL_CONTAINER_NAME)]..."
	@$(DOCKER) rm $(NR_LOCAL_CONTAINER_NAME) || true

## Start local node red
nr-local: nr-local-stop nr-local-remove nr-local-data-dir
	@echo "=> Starting local nodered server (image $(NR_IMAGE):$(NR_IMAGE_VERSION))..."
	$(DOCKER) run \
		-p $(NR_LOCAL_PORT):1880 \
		-v $(NR_LOCAL_DATA_DIR):/data \
		--name $(NR_LOCAL_CONTAINER_NAME) \
		$(NR_IMAGE_FULL)

## Shell into the local NodeRED isntance (if it's running)
nr-local-shell:
	@echo "=> Attempting to create shell (ash) into local nodered instance..."
	@$(DOCKER) exec -it $(NR_LOCAL_CONTAINER_NAME) /bin/ash

## Automatically rebuild and relaunch dockerized NodeRED on change to local files
nr-build-watch: ensure-tool-entr
	@find ./nigiri | $(ENTR) -rc $(MAKE) --quiet --no-print-directory nr-image nr-local


###################
# Flowforge Local #
###################
# Run a Flowforge (https://flowforge.com) enhanced NodeRED setup locally
# This setup uses a custom build (see docker/Dockerfile)
#

FF_NR_DOCKERFILE_PATH ?= $(realpath infra/docker/flowforge.Dockerfile)
FF_NR_IMAGE ?= ff-nr-local
FF_NR_IMAGE_VERSION ?= 2.2.2-12
FF_NR_IMAGE_FULL ?= $(FF_NR_IMAGE):$(FF_NR_IMAGE_VERSION)

FF_NR_LOCAL_CONTAINER_NAME ?= ff-nr-local

ff-nr-image:
	$(DOCKER) build -f $(FF_NR_DOCKERFILE_PATH) -t $(FF_NR_IMAGE_FULL) .

## Ensure local data directory for node red exists
ff-nr-local-data-dir:
	@echo "=> Ensuring local data dir for nodered @ [$(FF_NR_LOCAL_DATA_DIR)]..."
	@mkdir -p $(FF_NR_LOCAL_DATA_DIR)

## Stop local node red
ff-nr-local-stop:
	@echo "=> Stopping docker container [$(FF_NR_LOCAL_CONTAINER_NAME)]..."
	@$(DOCKER) stop $(FF_NR_LOCAL_CONTAINER_NAME) || true

## Remove local node-red container
ff-nr-local-remove:
	@echo "=> Removing docker container [$(FF_NR_LOCAL_CONTAINER_NAME)]..."
	@$(DOCKER) rm $(FF_NR_LOCAL_CONTAINER_NAME) || true

## Start local node red
ff-nr-local: ff-nr-local-stop ff-nr-local-remove
	@echo "=> Starting local nodered server (image $(FF_NR_IMAGE):$(FF_NR_IMAGE_VERSION))..."
	@$(DOCKER) run \
		-p $(FF_NR_LOCAL_PORT):1880 \
		-v $(FF_NR_LOCAL_DATA_DIR):/data \
		--name $(FF_NR_LOCAL_CONTAINER_NAME) \
		$(FF_NR_IMAGE_FULL)

## Shell into the local NodeRED isntance (if it's running)
ff-nr-local-shell:
	@echo "=> Attempting to create shell (ash) into local nodered instance..."
	@$(DOCKER) exec -it $(FF_NR_LOCAL_CONTAINER_NAME) /bin/ash

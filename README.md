# `nigiri` - A NodeRED module for ordering Sushi 🍣

This repo contains the [NodeRED][nr] code to set up a module for ordering Sushi online.

This module supports the following sushi providers:

- [Gin no Sara][ginnosara]

## How does it work?

To use this module you must:

- ✍🏾 Sign up for an account at [Gin no Sara][ginnosara] (this is required for [Cash on Delivery][wiki-cod] ordering)
- 🏠 Register your [delivery details](https://www.ginsara.jp/mypage/addresses/create)
- ➕ Add the custom `nigiri` NodeRED node to your Flow
- 🔧 Configure the `nigiri` node with:
  - 🏪 The numeric StoreID of the store closest to you (ex. 渋谷店 has ID `0164`)
  - 🍣 The numeric ProductID of the product you'd like to order (ex. a 紬(つむぎ) box has ID `303444`)
- ⚡ Trigger the `nigiri` node

### WARNING: This project uses Unsandboxed Chromium!

This project uses [puppeteer][pptr] with `chromium` and the `--no-sandbox` argument. Even if the docker container runs as an unprivileged user this can be unsafe while browsing the internet.

# Development

## Setup your environment

You can set up ENV variables to influence the environment.

If you're using [`direnv`](https://direnv.net), then an `.envrc` like the following:

```
export NR_IMAGE=registry.docker.com/your/docker/repo
```

This will controll the name (tag) that is built, which means you can also `docker push` the image (i.e., `make nr-image-publish`).

## Quickstart: Local NodeRED

You can start working with [NodeRED][nr] immediately by running the following:

```console
$ make nr-image nr-local
```

The NodeRED image that is run can either be the official NodeRED image, or our custom version. You can change which one is run by modifying your ENV/Make vars:

```console
export NR_IMAGE=nodered/node-red
export NR_IMAGE_VERSION=2.2.2
```

[wasm]: https://webassembly.org
[nr]: https://nodered.org
[flowforge]: https://flowforge.com/
[wiki-cod]: https://en.wikipedia.org/wiki/Cash_on_delivery
[ginnosara]: https://www.ginsara.jp
[pptr]: https://pptr.dev

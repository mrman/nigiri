FROM nr-builder:2.2.2-12

COPY . /usr/src/node-red/nigiri
WORKDIR /usr/src/node-red
RUN pnpm install /usr/src/node-red/nigiri

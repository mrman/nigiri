FROM nodered/node-red:2.2.2-12

# Root is required to execute chromium with --no-sandbox
USER root

RUN apk --update add chromium nss freetype harfbuzz ca-certificates ttf-freefont \
    terminus-font ttf-inconsolata ttf-dejavu \
    font-noto font-noto-cjk ttf-font-awesome font-noto-extra \
    font-sony-misc font-jis-misc

RUN apk add --no-cache curl fontconfig \
  && curl -O https://noto-website.storage.googleapis.com/pkgs/NotoSansCJKjp-hinted.zip \
  && mkdir -p /usr/share/fonts/NotoSansCJKjp \
  && unzip NotoSansCJKjp-hinted.zip -d /usr/share/fonts/NotoSansCJKjp/ \
  && rm NotoSansCJKjp-hinted.zip

RUN fc-cache -fv

# Node 12 only supports up to pnpm6
RUN npm install -g pnpm@6

ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true \
    PUPPETEER_EXECUTABLE_PATH=/usr/bin/chromium-browser

RUN addgroup -S pptruser && \
    mkdir -p /home/node-red/Downloads

# TODO(FIX): Unfortunately, root (w/ no-sandbox) must be used as namespacing fails in unprivileged mode
#
# (node:16) UnhandledPromiseRejectionWarning: Error: Failed to launch the browser process!
# Failed to move to new namespace: PID namespaces supported, Network namespace supported, but failed: errno = Operation not permitted
# [0722/123200.289928:FATAL:zygote_host_impl_linux.cc(191)] Check failed: ReceiveFixedMessage(fds[0], kZygoteBootMessage, sizeof(kZygoteBootMessage), &boot_pid).
# Received signal 6

# USER node-red

# Install nigiri
COPY --chown=node-red . /usr/src/node-red/nigiri
RUN cd /usr/src/node-red/nigiri && pnpm install

# Install the nigiri module so node-red can access it
WORKDIR /usr/src/node-red
RUN pnpm install /usr/src/node-red/nigiri
